const {model, Schema, ObjectId} = require('mongoose');

module.exports = model('user', new Schema({
    email: {type: String, required: true, unique:true},
    password: {type: String, required: true},
    diskSpace: {type: Number, default: 1024**3},
    usedSpace: {type: Number, default: 0},
    avatar: {type: String, default: '/img/no_avatar.png'},
    files: [{type: ObjectId, ref: 'File'}]
}));