const Router = require("express");
const router = new Router();
const UserController = require('../controllers/user.controller');
const auth = require('../middlewares/auth.middleware');
const { check } = require('express-validator');


// auth me
router.get('/me', auth, UserController.me);

// login on site
router.post('/login',UserController.login);

// registration on site
router.post('/registration',
    [check('email', 'Пустой или некорректный email').notEmpty().isEmail(),
        check('password', 'Пустой или короткий пароль (Должен быть длиннее 6 и короче 12 символов)')
            .notEmpty()
            .isLength({min: 6, max: 12})
    ], UserController.register);

module.exports = router;