const fs = require('fs');
const File = require('../models/File');
require('dotenv').config();

class FileService {
    createDir(req, file) {
        const filePath = `${req.filePath}/${file.user}${file.path}`; //file.path по умолчанию '/';
        return new Promise((resolve, reject) => {
            try {
                if (!fs.existsSync(filePath)) {
                    fs.mkdirSync(filePath, {recursive:true});
                    return resolve({ message: 'File was created' })
                } else {
                    reject({ message: 'Такой фаил уже добавлен' });
                }
            } catch (e) {
                console.log(e);
                return reject({ message: 'Ошибка добавления' })
            }
        })
    }
    // Скачивание файла
    async uploadFile(file, userId){
        return this.filePath(file, userId);
    }
    //Добавление файла
    async createFile(req, file, parent) {
        let path;
        return new Promise( async (resolve, reject) => {
            parent!==null ? path = `${req.filePath}/${req.user.id}${parent.path}${file.name}`
                          : path = `${req.filePath}/${req.user.id}/${file.name}`;
            if(fs.existsSync(path)) return reject({message: 'Такой фаил уже загружен в эту директорию'});
            try{
               await file.mv(path);
               return resolve({message: 'File is saved'});
            }catch (err){
                console.log(err);
                return reject({message: 'Ошибка сохранения файла на диске'})
            }
        })

    }

    //Удаление файла
    async deleteFile(req, file){
        const filePath = this.filePath(req, file);
        try{
            if (file.type === 'dir') {
                fs.rmdirSync(filePath);
            }else{
                fs.unlinkSync(filePath);
            }
            return true;
        }catch (e){
            console.log(e);
            return false;
        }
    }

    // Константа пути
    filePath (req, file) {
        return `${req.filePath}/${req.user.id}${file.path}`;
    }
}

module.exports = new FileService();