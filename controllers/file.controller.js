const fileService = require('../services/file.service');
const File = require('../models/File');
const User = require('../models/User');

class FileController {

    //Поиск файлов
    async searchFiles(req, res) {

        try{
            const search = req.query.search;
            let files= await File.find({user : req.user.id});
            files = files.filter(f => f.name.includes(search));
            res.json(files);
        }catch (e) {
            console.log(e);
            return res.status(500).json({message: 'Не удалось совершить поиск'})
        }

    }

    // Добавление файлов
    async addFile(req, res){
        try{
            let path;
            const file = req.files.file;
            const parent = await File.findOne({user: req.user.id, _id: req.body.parent});
            const user = await User.findById(req.user.id);
            if(parseFloat(user.usedSpace) + parseFloat(file.size) > user.diskSpace) {
                return res.status(400).json({message: 'Недостаточно места на диске'});
            }
            user.usedSpace = parseFloat(user.usedSpace) +  parseFloat(file.size);
            const newFile = {
                name: file.name,
                type: file.name.split('.').pop(),
                size: file.size,
                user: user._id,
                accessLink: false,
                path: `/${file.name}`
            }
            if(parent){
                newFile.path = `${parent.path}${file.name}`;
                newFile.parent = parent._id;
            }
            await fileService.createFile(req, file, parent);
            await user.save();
            const saveNewFileOnBase = await new File(newFile).save();
            if(parent){
                parent.child.push(saveNewFileOnBase._id);
                await parent.save();
            }
            return res.json(saveNewFileOnBase);
        } catch (e) {
            console.log(e);
            return res.status(400).json(e);
        }
    }

    // Скачивание файлов
    async uploadFile(req, res){
        try{
            const file = await File.findOne({_id:req.query.id, user: req.user.id});
            if(!file){ return res.status(404).json({message: 'Фаил не найден'}); }
            const currentPath = await fileService.uploadFile(req, file);
            return res.download(currentPath, file.name);
        }catch (err){
            console.log(err);
            return res.status(500).json({message: 'Ошибка загрузки данных'})
        }
    }

    // Добавление директорий
    async createDir(req, res) {
        try {
            const { name, type, parent } = req.body;
            const parentFile = await File.findOne({_id: parent}); //Ищем родителя
            const newFile = { name, type, user: req.user.id };
            if (parentFile){
                // with parent
                newFile['parent'] = parentFile._id;
                newFile['path'] = `${parentFile.path}${name}/`;
            } else {
                newFile['path'] = `/${name}/`;
            }
            await fileService.createDir(req, newFile);
            const file = await new File(newFile).save();
            if(parentFile) {
                parentFile.child.push(file._id);
                await parentFile.save();
            }
            res.json(file);
        } catch (e) {
            console.log(e);
            return res.status(400).json(e);
        }
    }

    //Выборка директорий
    async getDir(req, res) {
        try {
            let files;
            switch (req.query.sort){
                case 'name':
                    files = await File.find({ user: req.user.id, parent: req.query.parent }).sort({name:1});
                    break;
                case 'date':
                    files = await File.find({ user: req.user.id, parent: req.query.parent }).sort({date:1});
                    break;
                case 'type':
                    files = await File.find({ user: req.user.id, parent: req.query.parent }).sort({type:1});
                    break;
                default:
                    files = await File.find({ user: req.user.id, parent: req.query.parent });
            }

            return res.json(files)
        } catch (err) {
            console.log(err);
            return res.status(500).json({ message: 'Не удалось загрузить файлы' });
        }
    }

    // Удаление файлов и директорий
    async removeFile (req, res) {
        try{
            const file = await File.findOne({_id: req.query.id, user: req.user.id});
            if(!file){ return res.status(404).json({message: 'File or folder not founded'})};
            const removeStatus = await fileService.deleteFile(req, file);
            const removedFile = await file.remove();
            return res.json(removedFile);
        }catch (err){
            console.log(err);
            return res.status(500).json({message: 'Папка не пустая'})
        }
    }
}

module.exports = new FileController();