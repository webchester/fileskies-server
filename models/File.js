const { model, Schema, ObjectId } = require('mongoose');

module.exports = model('File', new Schema({
    name: { type: String, required: true },
    type: { type: String, default: "dir" },
    size: { type: Number },
    accessLink: { type: String},
    path: { type: String, required: true },
    date: { type: Date, default: Date.now() },
    user: { type: ObjectId, ref: 'User' },
    parent: { type: ObjectId, ref: 'File' },
    child: [{ type: ObjectId, ref: 'File' }]
}))