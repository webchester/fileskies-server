const bcrypt = require("bcrypt");
const User = require("../models/User");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const { validationResult } = require('express-validator');
const fileService = require('../services/file.service');
const File = require('../models/File');

class UserController{


     async login (req, res) {
            try{
                const {email, password} = req.body;
                const user = await User.findOne({email});
                if (!user) return res.status(400).json({message: 'Пользователь не найден'});
                if (!bcrypt.compareSync(password, user.password) ) return res
                    .status(400)
                    .json({message: 'Логин или пароль неверный'});
                const token = await jwt.sign({id: user._id, email: user.email},
                    process.env.SECRET_KEY,
                    {expiresIn: '12h'});
                res.json({
                    token,
                    user: {
                        id: user.id,
                        email: user.email,
                        diskSpace: user.diskSpace,
                        usedSpace: user.usedSpace,
                        avatar: user.avatar,
                        files: user.files
                    }});

            }catch(err){
                console.log(err.message | err);
                res.status(500).json({message: 'server error'})
            }
        }

     async register (req, res) {
            try{
                const validBodyFields = validationResult(req);
                if(!validBodyFields.isEmpty()) return res.status(400).json({message: validBodyFields});
                const {email, password} = req.body;
                const candidate =await User.findOne({email});
                if(candidate) return res.status(400).json({ message: `Пользователь с ${email} уже существует` });
                const hashPassword = await bcrypt.hash(password, 8);
                const user = new User({email, password: hashPassword});
                await fileService.createDir(req, new File({user: user._id, path: '/', name: ''}));
                await user.save();
                const token = await jwt.sign({id: user._id, email},
                    process.env.SECRET_KEY,
                    {expiresIn: '12h'});
                return res.json({token, user});
            }catch (err){
                console.log(err);
                return res.status(500).json({message: 'Ошибка регистрации пользователя'})
            }
        }

        async me(req, res) {
         try {
             const me = await User.findById(req.user.id);
            if(!me) return res.status(400).json({message: 'Пользователь не авторизован'});
            res.json({me})
         }catch (err) {
             res.status(500).json({message: 'Пользователь не авторизован'});
         }

        }

}

module.exports = new UserController();