const Router = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const router = new Router();
const fileController = require('../controllers/file.controller');

router.post('', authMiddleware, fileController.createDir);

router.get('', authMiddleware, fileController.getDir);

router.post('/upload', authMiddleware, fileController.addFile);

router.get('/download', authMiddleware, fileController.uploadFile);

router.delete('', authMiddleware, fileController.removeFile);

router.get('/search', authMiddleware, fileController.searchFiles);

module.exports = router;