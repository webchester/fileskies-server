const mongoose = require('mongoose');
const express= require('express');
const app = express();
const routes = require('./routes');
const cors = require('cors');
const fileUpload = require('express-fileupload');
require('dotenv').config();
const filepath = require('./middlewares/path.middleware');
const path = require('path');

app.use(filepath(path.resolve(__dirname, 'files')));

app.use(fileUpload({}));
app.use(express.static('public'));
app.use(cors());
app.use(express.json());
app.use(routes);


const PORT = process.env.PORT || 5000;
const main = async () => {
    try{
       await app.listen(PORT, () => {
            mongoose.connect(process.env.MONGO_URL,
                {useUnifiedTopology: true, useNewUrlParser: true});
            console.log(`SERVER STARTED ON PORT ${PORT}`);
        })
    }catch(err){
        console.log(err.message || err);
    }

}

main();