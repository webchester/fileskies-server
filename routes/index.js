const Router = require('express');
const rootRouter = new Router();
const userRouter = require('./user.router');
const fileRouter = require('./file.router');
const pathApi = '/api/v1/'

rootRouter.use(pathApi+'user', userRouter);
rootRouter.use(pathApi+'file', fileRouter);

module.exports = rootRouter;

